const { app, BrowserWindow, globalShortcut } = require("electron");

let win = null;

function createWindow() {
  win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
    },
    alwaysOnTop: false,
    titleBarStyle: "hidden",
    backgroundColor: "#b727ac",
  });

  win.loadURL("http://localhost:3000/");
}

function toggleDevTools() {
  win.webContents.openDevTools();
}

function createShortCuts() {
  globalShortcut.register("CmdOrCtrl+J", toggleDevTools);
}

app.whenReady().then(createWindow).then(createShortCuts);

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});
